//
//  User.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 08/06/2021.
//

import Foundation

final class User: Codable, Identifiable {
    //For list of users API and part of user profile API
      var login: String?
      var id: Int?
      var node_id: String?
      var avatar_url: String?
      var gravatar_id: String?
      var url: String?
      var html_url: String?
      var followers_url: String?
      var following_url: String?
      var gists_url: String?
      var starred_url: String?
      var subscriptions_url: String?
      var organizations_url: String?
      var repos_url: String?
      var events_url: String?
      var received_events_url: String?
      var type: String?
      var site_admin: Bool?
      
      //For list of users API
      var name: String?
      var company: String?
      var blog: String?
      var location: String?
      var email: String?
      var hireable: String?
      var bio:String?
      var twitter_username: String?
      var public_repos: Int?
      var public_gists: Int?
      var followers: Int?
      var following: Int?
      var created_at: String?
      var updated_at: String?

    #if DEBUG
    static func getUserList() -> [User] {
        let users = Bundle.main.decode([User].self, from: "users.json")
        return users
    }
    
    static func getUser() -> User {
        let example =  User()
        example.login = "mojombo"
        example.name = "Tom Preston-Werner"
        example.company = "@chatterbugapp, @redwoodjs, @preston-werner-ventures "
        example.blog = "https://tom.preston-werner.com/"
        example.location = "San Francisco"
        example.email = ""
        example.twitter_username = "twitter_username"
        example.followers = 22545
        example.following = 11
        example.node_id = "node_id"
        example.avatar_url = ""
        example.gravatar_id = ""
        example.url = ""
        example.html_url = ""
        example.followers_url = ""
        
        return example
    }
    
    #endif
}
