//
//  UserService.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 10/06/2021.
//

import Foundation

final class UserService {
    var baseURL = "https://api.github.com/"
    var url = ""
    var httpMethod: String?
    var postData: NSMutableDictionary?
    var responseData: Data?
    var timeout = 0
    var responseCode = 0
    var networkRetryCount = 0
    
    
    func getUsers(startID: Int, completionHandler: @escaping ([User]) -> ()) {
        self.url = "users?since=\(startID)"
        self.httpMethod = "GET"
        
        self.execute(completionHandler: { result in
            if let data = result {
                let decoder = JSONDecoder()
                
                if let users = try? decoder.decode([User].self, from: data) {
                    completionHandler(users)
                }
            }
        })
    }
    
    func getUserDetail(loginName: String, completionHandler: @escaping (User) -> ()) {
        self.url = "users/\(loginName)"
        self.httpMethod = "GET"
        
        self.execute(completionHandler: { result in
            if let data = result {
                let decoder = JSONDecoder()
                
                if let user = try? decoder.decode(User.self, from: data) {
                    DispatchQueue.main.async {
                        completionHandler(user)
                    }
                }
            }
        })
    }
    
    private func execute(completionHandler: @escaping (Data?) -> ()) {
        let url = URL(string: baseURL + url)!

        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(timeout)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        if let postData = postData,
           let httpBody = try? JSONSerialization.data(withJSONObject: postData, options: [.prettyPrinted]) {
            request.httpBody = httpBody
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("HTTP Request Failed \(error)")
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                self.responseCode = httpResponse.statusCode
                print("statusCode: \(httpResponse.statusCode)")
            }
            
            if let data = data {
                completionHandler(data)
            }
        }
        
        task.resume()
    }
}
