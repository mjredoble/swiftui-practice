//
//  ContentView.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 08/06/2021.
//

import SwiftUI

struct UsersView: View {
    
    @State var users: [User] = []
    
    var body: some View {
        NavigationView {
            List {
                ForEach(users) { user in
                    NavigationLink(destination: UserDetailView(user: user)) {
                        UserRowView(user: user)
                    }
                }
            }
            .navigationTitle("Users")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        UsersView(users: User.getUserList())
    }
}
