//
//  UserInformationRowView.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 10/06/2021.
//

import SwiftUI

struct UserInformationRowView: View {
    
    var user: User
    var information = ["Name:", "Company", "Location", "Twitter", "Github"]
    
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: 10, content: {
                Text("\(information[0])")
                    .font(.callout)
                Text("\t\(user.name ?? "-")")
                    .font(.headline)
                Spacer()
            })
            
            HStack(alignment: .top, spacing: 10, content: {
                Text("\(information[1])")
                    .font(.callout)
                Text("\t\(user.company ?? "-")")
                    .font(.headline)
                Spacer()
            })
            
            HStack(alignment: .top, spacing: 10, content: {
                Text("\(information[2])")
                    .font(.callout)
                Text("\t\(user.location ?? "-")")
                    .font(.headline)
                Spacer()
            })
            
            HStack(alignment: .top, spacing: 10, content: {
                Text("\(information[3])")
                    .font(.callout)
                Text("\t\(user.twitter_username ?? "-")")
                    .font(.headline)
                Spacer()
            })
            
            HStack(alignment: .top, spacing: 10, content: {
                Text("\(information[4])")
                    .font(.callout)
                Text("\t\(user.url ?? "-")")
                    .font(.headline)
                Spacer()
            })
        }
    }
}

struct UserInformationRowView_Previews: PreviewProvider {
    static var previews: some View {
        UserInformationRowView(user: User.getUser())
    }
}
