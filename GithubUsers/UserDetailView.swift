//
//  UserDetailView.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 10/06/2021.
//

import SwiftUI

struct UserDetailView: View {
    
    @State var user: User
    
    @State private var notes = ""
    
    var body: some View {
        VStack {
            Image("logo")
                .resizable()
                .scaledToFit()
            
            Form {
                Section(header: Text("Information")) {
                    UserInformationRowView(user: user)
                }
                
                Section(header: Text("Notes")) {
                    TextField("Notes", text: $notes)
                }
            }
        }
        .navigationTitle(user.login ?? "Login Name")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
            UserService().getUserDetail(loginName: user.login ?? "", completionHandler: { user in
                self.user = user
            })
        }
    }
    
}

struct UserDetailView_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailView(user: User.getUser())
    }
}
