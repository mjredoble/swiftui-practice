//
//  UserRow.swift
//  GithubUsers
//
//  Created by Michael Joseph Redoble on 10/06/2021.
//

import SwiftUI

struct UserRowView: View {
    let user: User
    
    var body: some View {
        HStack {
            //Image should be fetch from URL avatar_url
            Image("logo_thumbnail")
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray, lineWidth: 2))
            
            VStack(alignment: .leading) {
                Text(user.login!)
                    .font(.headline)
                Text(user.url!)
                    .font(.caption)
            }
            
            //This will automatically take up spaces.
            Spacer()
        }
    }
}

struct UserRow_Previews: PreviewProvider {
    static var previews: some View {
        UserRowView(user: User.getUser())
    }
}
